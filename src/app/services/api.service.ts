import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Response } from '@angular/http/src/static_response';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { Game } from '../models/game';
import { Serie } from '../models/serie';
import { CommonService } from './common.service';
import { DebugService } from './debug.service';
import { Hero as DotaHero } from '../models/dota-2/hero';

@Injectable()
export class ApiService {
    constructor(private _http: Http, private _commonService: CommonService, private _debugService: DebugService) {}

    getChampions(): Observable<Response> {
        return this._http.get(this._commonService.getUrl() + 'lol/champions.json?' + this._commonService.getApiToken());
    }
    getChampionsJSON(): Observable<Response> {
        return this._http.get('./assets/lolChampions.json');
    }
    getGames(): Observable<Game[]> {
        const url = this._debugService.getDebug() ? '/assets/api/games.json' : this._commonService.getUrl() + 'videogames.json?'
            + this._commonService.getApiToken();
        return this._http.get(url).map(response => {
            if (this._commonService.isResponseStatusOk(response)) {
                return <Game[]>response.json();
            } else {
                return [];
            }
        });
    }

    getDotaHeroes() {
        const service = 'dota2/heroes.json';
        const url = this._debugService.getDebug() ? `/assets/api/${service}` :
        `${this._commonService.getUrl()}${service}?${this._commonService.getApiToken()}`;
        return this._http.get(url).map(response => {
            if (this._commonService.isResponseStatusOk(response)) {
                return <DotaHero[]>response.json();
            } else {
                return [];
            }
        });
    }
}

