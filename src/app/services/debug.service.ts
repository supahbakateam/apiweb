import { Injectable } from '@angular/core';
import { Response } from '@angular/http/src/static_response';

@Injectable()
export class DebugService {
    private _debug = true;

    getDebug() {
        return this._debug;
    }
}

