import { Injectable } from '@angular/core';
import { Response } from '@angular/http/src/static_response';

@Injectable()
export class CommonService {

    private _url = 'https://api.pandascore.co/';
    private _apiToken = 'token=lxBqOmT_rF8nu_Cvj0pVqj10AwblczSb_9sIHeTcxAqJeUR5VwA';

    getUrl() {
        return this._url;
    }

    getApiToken() {
        return this._apiToken;
    }

    isResponseStatusOk(response: Response): boolean {
        return (response && response.ok && response.status >= 200 && response.status < 300)
            ? true : false;
    }
}

