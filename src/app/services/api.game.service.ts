import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Response } from '@angular/http/src/static_response';
import { Observable } from 'rxjs/Observable';

import { Game } from '../models/game';
import { Serie } from '../models/serie';
import { CommonService } from './common.service';
import { DebugService } from './debug.service';
import { Tournament } from '../models/tournament';
import { League } from '../models/league';

@Injectable()
export class ApiGameService {
    private _apiToken: string;
    private _url: string;
    private _game: Game;

    constructor(private _http: Http, private _commonService: CommonService, private _debugService: DebugService) { }

    getGame(): Game {
        return this._game;
    }

    setGame(game: Game): void {
        this._game = game;
    }
    /**
     * @description Gets the series from a specific game
     * @param gameId number or string
     * @returns series
     */
    getSeries(gameId: number | string): Observable<Serie[]> {
        const url = this._debugService.getDebug() ? '/assets/api/series.json'
            : this._commonService.getUrl() + 'videogames/' + gameId + '/series.json?' + this._commonService.getApiToken();

        return this._http.get(url).map(response => {
            if (this._commonService.isResponseStatusOk(response)) {
                return <Serie[]>response.json();
            } else {
                return [];
            }
        });
    }

    /**
      * @description Gets the tournaments from a specific game
      * @param gameId number or string
      * @returns tounrnaments
      */
    getTournaments(gameId: number | string): Observable<Tournament[]> {
        const url = this._debugService.getDebug() ? 'assets/api/tournaments.json'
            : this._commonService.getUrl() + 'videogames/' + gameId + '/tournaments.json?' + this._commonService.getApiToken();

        return this._http.get(url).map(response => {
            if (this._commonService.isResponseStatusOk(response)) {
                return <Tournament[]>response.json();
            } else {
                return [];
            }
        });
    }

    /**
      * @description Gets the leagues from a specific game
      * @param gameId number or string
      * @returns leagues
      */
    getLeagues(gameId: number | string): Observable<League[]> {
        const url = this._debugService.getDebug() ? 'assets/api/leagues.json'
            : this._commonService.getUrl() + 'videogames/' + gameId + '/leagues.json?' + this._commonService.getApiToken();
        return this._http.get(url).map(response => {
            if (this._commonService.isResponseStatusOk(response)) {
                return <League[]>response.json();
            } else {
                return [];
            }
        });
    }

}

