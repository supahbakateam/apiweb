import { Injectable } from '@angular/core';

import { LolChampion } from '../models/lol-champion';

@Injectable()
export class LolChampionService {
    champions: LolChampion[];
    constructor() {
        this.champions = [];

    }
    GetAll(): LolChampion[] {
        return this.champions;
    }

    GetElement(id: number): LolChampion {
        return this.champions.filter(e => e.id === id)[0];
    }

    Organise(field: string = 'name'): void {
        this.champions.sort((a: any, b: any) => {
            if (a[field] < b[field]) {
                return -1;
            } else if (a[field] > b[field]) {
                return 1;
            } else {
                return 0;
            }
        });
    }

    SetElement(champion): void {
        this.champions.push(champion);
    }

    DeleteAll(): void {
        this.champions = [];
    }
}
