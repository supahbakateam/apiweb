import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { Game } from '../../models/game';
import { ApiGameService } from '../../services/api.game.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss']
})
export class GameCardComponent implements OnInit, OnChanges {
  imgUrl: string;

  @Input()
  public game: Game;

  constructor(private router: Router, private _apiGameService: ApiGameService) { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.game) {
      this.imgUrl = `url(/assets/games/${this.game.slug}.jpg)`;
    }
  }

  goToGame() {
    this._apiGameService.setGame(this.game);
    this.router.navigate(['/game', this.game.slug]);
  }
}
