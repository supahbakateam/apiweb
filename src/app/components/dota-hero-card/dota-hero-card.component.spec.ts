import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotaHeroCardComponent } from './dota-hero-card.component';

describe('DotaHeroCardComponent', () => {
  let component: DotaHeroCardComponent;
  let fixture: ComponentFixture<DotaHeroCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotaHeroCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotaHeroCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
