import { Component, OnInit, Input } from '@angular/core';
import { Hero } from '../../models/dota-2/hero';

@Component({
    selector: 'app-dota-hero-card',
    templateUrl: './dota-hero-card.component.html',
    styleUrls: ['./dota-hero-card.component.scss']
})
export class DotaHeroCardComponent implements OnInit {
    @Input()
    hero: Hero;

    constructor() { }

    ngOnInit() {
    }

}
