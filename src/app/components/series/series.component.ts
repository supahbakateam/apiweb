import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Serie } from '../../models/serie';
import { ApiGameService } from '../../services/api.game.service';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.scss']
})
export class SeriesComponent implements OnInit {
  series: Serie[] = [];

  constructor(private _activatedRoute: ActivatedRoute, private _apiGameService: ApiGameService) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe((params: Params) => {
      const slug = params['slug'];
      this._apiGameService.getSeries(slug)
        .subscribe(series => {
          this.series = series;
        });
    });
  }
}
