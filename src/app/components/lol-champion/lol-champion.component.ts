import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';
import { LolChampion } from '../../models/lol-champion';

@Component({
  selector: 'app-lol-champion',
  templateUrl: './lol-champion.component.html',
  styleUrls: ['./lol-champion.component.scss']
})
export class LolChampionComponent implements OnInit, OnChanges {
  imgUrl: string;

  @Input()
  public champion: LolChampion;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.champion) {
        this.imgUrl = `url(https://ddragon.leagueoflegends.com/cdn/img/champion/splash/${this.champion.image}_0.jpg)`;
    }
}

}
