import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LolChampionComponent } from './lol-champion.component';

describe('LolChampionComponent', () => {
  let component: LolChampionComponent;
  let fixture: ComponentFixture<LolChampionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LolChampionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LolChampionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
