import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LolChampionDetailComponent } from './lol-champion-detail.component';

describe('LolChampionDetailComponent', () => {
  let component: LolChampionDetailComponent;
  let fixture: ComponentFixture<LolChampionDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LolChampionDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LolChampionDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
