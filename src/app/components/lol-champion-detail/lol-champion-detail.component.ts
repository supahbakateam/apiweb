import { Component, OnInit } from '@angular/core';

import { LolChampion } from '../../models/lol-champion';
import { LolChampionService } from '../../services/lol-champion.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-lol-champion-detail',
  templateUrl: './lol-champion-detail.component.html',
  styleUrls: ['./lol-champion-detail.component.scss']
})
export class LolChampionDetailComponent implements OnInit {
  champion: LolChampion;
  championId: string;
  championImage: string;
  championImageSkin: string[] = [];
  constructor(private _router: Router, private _activatedRoute: ActivatedRoute, private _lolChampionService: LolChampionService) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe(params => {
      this.championId = params['id'];
      this.champion = <LolChampion>this._lolChampionService.GetElement(parseInt(this.championId, 10));
      this.championImage = this.champion.imageVertical;
      /*
      let count = 0;
      let image;
      do {
        image = 'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + this.champion.image + '_' + count + '.jpg';
        this.championImageSkin.push(image);
        count++;
        image = 'https://ddragon.leagueoflegends.com/cdn/img/champion/splash/' + this.champion.image + '_' + count + '.jpg';
      }
      while (this.imageExists(image));
      */
  });
  }
  ClickReturn() {
    this._router.navigate(['/lol']);
  }
  imageExists(image_url) {

    const http = new XMLHttpRequest();
    http.open('HEAD', image_url, false);
    http.send();

    return http.statusText === 'OK';
  }
}
