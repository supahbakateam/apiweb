import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Hero } from '../../models/dota-2/hero';

@Component({
    selector: 'app-dota-heroes',
    templateUrl: './dota-heroes.component.html',
    styleUrls: ['./dota-heroes.component.scss']
})
export class DotaHeroesComponent implements OnInit {
    heroes: Hero[];

    constructor(private apiService: ApiService) { }

    ngOnInit() {
        this.apiService.getDotaHeroes().subscribe(result => {
            if (result) {
                result.forEach(hero => {
                    hero.image_url = `url(${hero.image_url})`;
                });
                this.heroes = result;
            }
        });
    }

}
