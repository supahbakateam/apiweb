import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DotaHeroesComponent } from './dota-heroes.component';

describe('DotaHeroesComponent', () => {
  let component: DotaHeroesComponent;
  let fixture: ComponentFixture<DotaHeroesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DotaHeroesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DotaHeroesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
