import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(private _apiService: ApiService) { }

  ngOnInit() {
    this.getChampions();
  }

  /**
   * Get champions
   */
  getChampions() {
    this._apiService.getChampionsJSON().subscribe(resp => {
      console.log(resp.json());
    });
  }


}
