import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Game } from '../../models/game';

@Component({
    selector: 'app-game-list',
    templateUrl: './game-list.component.html',
    styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {
    private isLoading: boolean;
    private games: Game[] = [];

    constructor(private apiService: ApiService) {
    }

    ngOnInit() {
        this.isLoading = true;
        this.apiService.getGames().subscribe(games => {
            this.games = games;
            this.isLoading = false;
        });
    }

}
