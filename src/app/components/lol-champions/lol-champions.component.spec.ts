import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LolChampionsComponent } from './lol-champions.component';

describe('LolChampionsComponent', () => {
  let component: LolChampionsComponent;
  let fixture: ComponentFixture<LolChampionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LolChampionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LolChampionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
