import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';

import { LolChampion } from '../../models/lol-champion';
import { LolChampionService } from '../../services/lol-champion.service';

@Component({
  selector: 'app-lol-champions',
  templateUrl: './lol-champions.component.html',
  styleUrls: ['./lol-champions.component.scss'],
})
export class LolChampionsComponent implements OnInit {

  champions: LolChampion[];
  constructor(private _apiService: ApiService, private _router: Router, private _lolChampionService: LolChampionService) { }

  ngOnInit() {
    this.getChampions();
  }

  /**
   * Get champions
   */
  getChampions() {
    this._apiService.getChampionsJSON().subscribe(resp => {
      this._lolChampionService.DeleteAll();
      // console.log(resp.json());
      for (const perso of resp.json()) {
        // var champion = { id: perso.id, name: perso.name, image: perso.big_image_url ,imageIcon: perso.image_url };
        let tempNameList = perso.big_image_url.split('/');
        let tempName = tempNameList[tempNameList.length - 1];
        tempNameList = tempName.split('_');
        tempName = tempNameList[0];
        const champion = {
          id: perso.id,
          name: perso.name,
          image: tempName,
          imageVertical: perso.big_image_url,
          imageProfil: perso.image_url
        };
        // https://ddragon.leagueoflegends.com/cdn/img/champion/splash/
        this._lolChampionService.SetElement(champion);
      }

      this._lolChampionService.Organise();
      this.champions = this._lolChampionService.GetAll();

    });
  }

  ClickChampion(champion: LolChampion) {
    this._router.navigate(['/lol', champion.id]);
  }

}
