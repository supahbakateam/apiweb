import { Component, OnInit } from '@angular/core';
import { Game } from '../../models/game';
import { Tournament } from '../../models/tournament';
import { League } from '../../models/league';
import { Serie } from '../../models/serie';
import { ApiGameService } from '../../services/api.game.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-game',
    templateUrl: './game.component.html',
    styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
    public game: Game;
    public tournaments: Tournament[] = [];
    public leagues: League[] = [];
    public series: Serie[] = [];

    constructor(
        private router: Router,
        private _activatedRoute: ActivatedRoute,
        private _apiGameService: ApiGameService) { }

    ngOnInit(): void {
        this._activatedRoute.params.subscribe(routes => {
            const gameId = routes['slug'];

            if (!this._apiGameService.getGame() || this._apiGameService.getGame().slug !== gameId) {
                this.router.navigate(['/game']);
            }

            this.game = this._apiGameService.getGame();
            this._apiGameService.getLeagues(gameId).subscribe(leagues => this.leagues = leagues);
            this._apiGameService.getSeries(gameId).subscribe(series => this.series = series);
            this._apiGameService.getTournaments(gameId).subscribe(tournaments => this.tournaments = tournaments);
        });
    }
}
