import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NG_ASYNC_VALIDATORS, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';

import { appRoutes } from './appRoutes';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { ApiService } from './services/api.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LolChampionsComponent } from './components/lol-champions/lol-champions.component';
import { LolChampionDetailComponent } from './components/lol-champion-detail/lol-champion-detail.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { LolChampionService } from './services/lol-champion.service';
import { AppModuleMaterial } from './app.module-material';
import { ToolbarComponent } from './components/shared/toolbar/toolbar.component';
import { GameComponent } from './components/game/game.component';
import { GameListComponent } from './components/game-list/game-list.component';
import { SeriesComponent } from './components/series/series.component';
import { GameCardComponent } from './components/game-card/game-card.component';
import { ApiGameService } from './services/api.game.service';
import { DebugService } from './services/debug.service';
import { CommonService } from './services/common.service';
import { LolChampionComponent } from './components/lol-champion/lol-champion.component';
import { DotaHeroesComponent } from './components/dota-heroes/dota-heroes.component';
import { DotaHeroCardComponent } from './components/dota-hero-card/dota-hero-card.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LolChampionsComponent,
    LolChampionDetailComponent,
    NotFoundComponent,
    ToolbarComponent,
    GameComponent,
    GameListComponent,
    SeriesComponent,
    GameCardComponent,
    LolChampionComponent,
    DotaHeroesComponent,
    DotaHeroCardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppModuleMaterial,
    FlexLayoutModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    ApiService,
    ApiGameService,
    DebugService,
    CommonService,
    LolChampionService ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
