import { Routes } from '@angular/router';
import { LolChampionsComponent } from './components/lol-champions/lol-champions.component';
import { LolChampionDetailComponent } from './components/lol-champion-detail/lol-champion-detail.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GameListComponent } from './components/game-list/game-list.component';
import { GameComponent } from './components/game/game.component';
import { SeriesComponent } from './components/series/series.component';
import { DotaHeroesComponent } from './components/dota-heroes/dota-heroes.component';

export const appRoutes: Routes = [
    { path: '', component: DashboardComponent },
    { path: 'game', component: GameListComponent },
    { path: 'game/:slug', component: GameComponent },
    { path: 'game/:slug/series', component: SeriesComponent },
    { path: 'game/dota-2/heroes', component: DotaHeroesComponent },
    { path: 'lol', component: LolChampionsComponent },
    { path: 'lol/:id', component: LolChampionDetailComponent },
    { path: 'not-found', component: NotFoundComponent },
    { path: '**', redirectTo: '/not-found' }
];
