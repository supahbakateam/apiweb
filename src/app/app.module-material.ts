import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatIconModule,
    MatCheckboxModule,
    MatAutocompleteModule,
    MatButtonToggleModule,
    MatInputModule,
    MatGridListModule,
    MatDialogModule,
    MatSnackBarModule,
    MatTabsModule,
} from '@angular/material';


@NgModule({
    imports: [
        MatButtonModule,
        MatCardModule,
        MatMenuModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatIconModule,
        MatCheckboxModule,
        MatAutocompleteModule,
        MatButtonToggleModule,
        MatInputModule,
        MatGridListModule,
        MatDialogModule,
        MatSnackBarModule,
        MatTabsModule,
        BrowserAnimationsModule
    ],
    exports: [
        MatButtonModule,
        MatCardModule,
        MatMenuModule,
        MatFormFieldModule,
        MatToolbarModule,
        MatIconModule,
        MatCheckboxModule,
        MatAutocompleteModule,
        MatButtonToggleModule,
        MatInputModule,
        MatGridListModule,
        MatDialogModule,
        MatSnackBarModule,
        MatTabsModule,
        BrowserAnimationsModule
    ]
})
export class AppModuleMaterial {
}
