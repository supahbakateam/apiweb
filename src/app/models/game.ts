import { League } from './league';

export interface Game {
    id: number;
    slug: string;
    name: string;
    leagues: League[];
}
