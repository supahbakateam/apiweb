import { League } from './league';
import { Game } from './game';
import { Tournament } from './tournament';

export interface Serie {
    id: number;
    name: string;
    slug: string;
    // todo: season: Season,
    beginAt: string;
    description: string;
    prizepool: string;
    league_id: number;
    league: League;
    videogame: Game;
    tournaments: Tournament[];
}
