export interface Hero {
    id: number;
    localized_name: string;
    image_url: string;
}
