export interface LolChampion {
    id: number;
    name: string;
    image: string;
    imageVertical: string;
    imageProfil: string;
}
