export interface League {
    id: number;
    image_url: string;
    url: string;
    name: string;
    slug: string;
}
